package com.example.phonecontact;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.phonecontact.helper.FragmentHelper;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.*;

public class ContactListFragment extends Fragment {
    private customContactAdapter adapter;
    private List<Contact> data;
    private RecyclerView recyclerView;
    private Button btnAdd;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        recyclerView=container.findViewById(R.id.myRecyclerView);
        return inflater.inflate(R.layout.contact_list,null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.myRecyclerView);
        data=new ArrayList<>();
        for (int i=1;i<20;i++){
            data.add(new Contact("Contact 00"+i,"9090909090",null));
        }
        adapter=new customContactAdapter(data);

        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity(), VERTICAL,false);
       recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
