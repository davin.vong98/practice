package com.example.phonecontact;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.phonecontact.helper.FragmentHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Contact> data=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button btnAddContact;
        ContactListFragment contactListFragment=new ContactListFragment();
        FragmentHelper.add(this,R.id.contact_container,contactListFragment);
        btnAddContact=findViewById(R.id.btnAddContact);
        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewContactFragment contactFragment=new NewContactFragment();
                FragmentHelper.replace(MainActivity.this,R.id.main_container,contactFragment);
            }
        });
    }
}
